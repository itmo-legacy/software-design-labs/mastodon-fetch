{-# OPTIONS_GHC -fno-cse #-}

{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE RecordWildCards    #-}

module Main where

import Data.Time (getCurrentTime, nominalDay)
import System.Console.CmdArgs.Implicit
  (Data, Typeable, cmdArgs, def, help, program, summary, (&=))

import Business (breakDownHashtags)
import HashtagOccurrence (Hashtag(..))
import IOInterpreter (runRequestL)

data MastodonFetch = MastodonFetch
  { hashtag :: String
  , hours   :: Int
  } deriving (Data, Show, Typeable)

mastodonFetch :: MastodonFetch
mastodonFetch = MastodonFetch
  { hashtag = def &= help "Hashtag to search for"
  , hours = def &= help "Number of hours to account for"
  }
  &= program "mastodon-fetch"
  &= summary "Break down number of statuses with the hashtag by hours"

main :: IO ()
main = do
  MastodonFetch{..} <- cmdArgs mastodonFetch
  currentTime <- getCurrentTime
  let nominalHour = nominalDay / 24
  occurences <- runRequestL $
    breakDownHashtags nominalHour hours currentTime (Hashtag hashtag)
  let frequencies = map length occurences
  print frequencies
