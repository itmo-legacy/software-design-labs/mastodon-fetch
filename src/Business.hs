{-# LANGUAGE BangPatterns     #-}
{-# LANGUAGE FlexibleContexts #-}

module Business
  ( breakDownHashtags
  ) where

import Control.Monad.Writer (execWriterT, tell)
import Data.Function ((&))
import Data.List (sortOn)
import Data.Time (NominalDiffTime, UTCTime, addUTCTime)

import HashtagOccurrence (Hashtag, HashtagOccurrence(..))
import Request (FetchHashtagConfig(..), RequestL, fetchHashtag)

batchSize :: Int
batchSize = 100

fetchHashtagSince :: UTCTime -> Hashtag -> RequestL [HashtagOccurrence]
fetchHashtagSince time hashtag = execWriterT $ fetchInBatch FetchHashtagConfig
  { fhcMaxId = Nothing
  , fhcLimit = batchSize
  , fhcHashtag = hashtag
  }
  where
    fetchInBatch cfg = do
      hashtags <- fetchHashtag cfg
      let (old, new) = hashtags & sortOn hoDate & span ((< time) . hoDate)
      case (new, old) of
        (n : _, []) -> fetchInBatch cfg{ fhcMaxId = Just (hoStatusId n)}
        _           -> pure ()
      tell new

fetchHashtagInPeriod
  :: UTCTime -> UTCTime -> Hashtag -> RequestL [HashtagOccurrence]
fetchHashtagInPeriod since upto hashtag
  = takeWhile ((<= upto) . hoDate) <$> fetchHashtagSince since hashtag

breakDownHashtags
  :: NominalDiffTime
  -> Int
  -> UTCTime
  -> Hashtag
  -> RequestL [[HashtagOccurrence]]
breakDownHashtags period times upto hashtag = do
  allHashtags <- fetchHashtagInPeriod since upto hashtag
  pure (assortToTimespans allHashtags timespans)
  where
    since = addUTCTime (-period * fromIntegral times) upto
    starts = iterate (addUTCTime period) since
    fins = tail starts
    timespans = take times (zip starts fins)

type Timespan = (UTCTime, UTCTime)

assortToTimespans :: [HashtagOccurrence] -> [Timespan] -> [[HashtagOccurrence]]
assortToTimespans occurrences = reverse . snd . foldl folder (occurrences, [])
  where
    folder (!occs, !acc) (st, fin) = (newer, suitable : acc)
      where
        (_older, rest) = span ((< st) . hoDate) occs
        (suitable, newer) = span ((< fin) . hoDate) rest
