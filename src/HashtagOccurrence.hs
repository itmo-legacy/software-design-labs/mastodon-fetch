{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings          #-}

module HashtagOccurrence
  ( Hashtag (..)
  , HashtagOccurrence (..)
  , StatusId (..)
  ) where

import Data.Aeson (FromJSON(..), withObject, (.:))
import Data.Time (UTCTime, zonedTimeToUTC)

newtype Hashtag = Hashtag String
  deriving Show

newtype StatusId = StatusId String
  deriving (Eq, Show, FromJSON)

data HashtagOccurrence = HashtagOccurrence
  { hoStatusId :: StatusId
  , hoDate     :: UTCTime
  } deriving (Show, Eq)

instance FromJSON HashtagOccurrence where
  parseJSON = withObject "Hashtag" $ \v -> HashtagOccurrence
    <$> v .: "id"
    <*> (zonedTimeToUTC <$> v .: "created_at")
