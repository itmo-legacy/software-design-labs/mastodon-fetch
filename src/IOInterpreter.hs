{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}

module IOInterpreter
  ( runRequestL
  ) where

import Control.Monad.Free.Church (foldF)
import Data.ByteString.Builder (intDec, stringUtf8, toLazyByteString)
import Data.ByteString.Lazy (toStrict)
import Data.Coerce (coerce)
import Data.Functor ((<&>))
import Network.HTTP.Simple (Request, getResponseBody, httpJSON, parseRequest, setRequestQueryString)

import HashtagOccurrence (Hashtag(..), StatusId(..))
import Request (FetchHashtagConfig(..), RequestF(..), RequestL)

runRequestF :: RequestF a -> IO a
runRequestF (FetchHashtag cfg cont) = do
  request <- makeFetchHashtagRequest cfg
  response <- httpJSON request
  pure (cont (getResponseBody response))

makeFetchHashtagRequest :: FetchHashtagConfig -> IO Request
makeFetchHashtagRequest FetchHashtagConfig{..}
  = parseRequest ("GET " <> base <> coerce fhcHashtag)
  <&> setRequestQueryString [("max_id", maxId), ("limit", Just limit)]
  where
    base = "https://mastodon.social/api/v1/timelines/tag/"
    maxId = fmap (toStrictByteString . stringUtf8) (coerce fhcMaxId)
    limit = toStrictByteString (intDec fhcLimit)
    toStrictByteString = toStrict . toLazyByteString

runRequestL :: RequestL a -> IO a
runRequestL = foldF runRequestF
