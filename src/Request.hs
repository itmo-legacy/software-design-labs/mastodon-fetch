{-# LANGUAGE DeriveFunctor    #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GADTs            #-}
{-# LANGUAGE TemplateHaskell  #-}

module Request
  ( RequestF (..)
  , RequestL

  , FetchHashtagConfig (..)

  , fetchHashtag
  ) where

import Control.Monad.Free.Church (F(..), MonadFree, liftF)
import Control.Monad.Free.TH (makeFree)

import HashtagOccurrence (Hashtag, HashtagOccurrence, StatusId)

data FetchHashtagConfig = FetchHashtagConfig
  { fhcMaxId   :: Maybe StatusId
  , fhcLimit   :: Int
  , fhcHashtag :: Hashtag
  } deriving Show

data RequestF next where
  FetchHashtag :: FetchHashtagConfig -> ([HashtagOccurrence] -> next) -> RequestF next
  deriving Functor

type RequestL = F RequestF

makeFree ''RequestF
