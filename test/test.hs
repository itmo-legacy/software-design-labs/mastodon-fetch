{-# LANGUAGE RecordWildCards            #-}

import Control.Monad.Free.Church (foldF)
import Data.Function ((&))
import Data.Functor.Identity (Identity(..))
import Data.Time (NominalDiffTime, UTCTime(..), addUTCTime, nominalDay)
import Data.Time.Calendar.OrdinalDate (fromOrdinalDate)
import Test.Hspec.Expectations (shouldBe)
import Test.Tasty (TestTree, defaultMain, testGroup)
import Test.Tasty.HUnit (Assertion, testCase)

import Business (breakDownHashtags)
import HashtagOccurrence (Hashtag(..), HashtagOccurrence(..), StatusId(..))
import Request (FetchHashtagConfig(..), RequestF(..), RequestL)

baseTime :: UTCTime
baseTime = UTCTime (fromOrdinalDate 1970 1) 0

anHour :: NominalDiffTime
anHour = nominalDay / 24

hashtagOccurrences :: [HashtagOccurrence]
hashtagOccurrences =
  -- first hour
  [ HashtagOccurrence (StatusId "1") baseTime
  , HashtagOccurrence (StatusId "2") (addUTCTime (anHour * 0.5) baseTime)
  , HashtagOccurrence (StatusId "3") (addUTCTime (anHour * 0.6) baseTime)

  -- second hour
  , HashtagOccurrence (StatusId "4") (addUTCTime anHour baseTime)
  , HashtagOccurrence (StatusId "5") (addUTCTime (anHour * 1.55) baseTime)

  -- third hour, no hashtags

  -- fourth hour
  , HashtagOccurrence (StatusId "6") (addUTCTime (3 * anHour) baseTime)
  , HashtagOccurrence (StatusId "7") (addUTCTime (3 * anHour) baseTime)
  ]

main :: IO ()
main = defaultMain tests

tests :: TestTree
tests = testGroup "count-hashtags"
  [ testCase "test-frequencies" testFrequencies ]

testFrequencies :: Assertion
testFrequencies = do
  let occurrences = runIdentity (testRequestL breakDown)
  let frequencies = map length occurrences
  frequencies `shouldBe` [3, 2, 0, 2]
  where
    breakDown
      = breakDownHashtags anHour 4
        (addUTCTime (anHour * 4) baseTime)
        (Hashtag "doesn't matter")

statusCmp :: StatusId -> StatusId -> Ordering
statusCmp (StatusId s1) (StatusId s2) = compare (zFill s1) (zFill s2)
  where
    zFill s = replicate (length s - maxL) '0' <> s
    maxL = max (length s1) (length s2)

testRequestF :: RequestF a -> Identity a
testRequestF (FetchHashtag FetchHashtagConfig{..} cont)
  = hashtagOccurrences
  & takeWhile isSuitable
  & take fhcLimit
  & cont & pure
  where
    isSuitable
      | Just maxId <- fhcMaxId = \occ -> statusCmp (hoStatusId occ) maxId == LT
      | otherwise = const True

testRequestL :: RequestL a -> Identity a
testRequestL = foldF testRequestF
